$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    var relatedTarget;
    $('#reserve').on('show.bs.modal', function(e) {
        console.log('El modal se está abriendo...');
        relatedTarget = $(e.relatedTarget);
        relatedTarget.prop('disabled', true);
        relatedTarget.removeClass('btn-primary').addClass('btn-secondary');
    });
    $('#reserve').on('shown.bs.modal', function(e) {
        console.log('El modal se abrió...');
    });
    $('#reserve').on('hide.bs.modal', function(e) {
        console.log('El modal se está ocultando...');
    });
    $('#reserve').on('hidden.bs.modal', function(e) {
        console.log('El modal se ocultó...');
        relatedTarget.prop('disabled', false);
        relatedTarget.removeClass('btn-secondary').addClass('btn-primary');
        relatedTarget = null;
    });
});